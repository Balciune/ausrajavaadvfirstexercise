public class Person {
    private String firstName;
    private String secondName;
    private String dataOfBirth;


    public Person(String firstName, String secondName, String dataOfBirth) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.dataOfBirth = dataOfBirth;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return this.secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getDataOfBirth() {
        return this.dataOfBirth;
    }

    public void setDataOfBirth(String dataOfBirth) {
        this.dataOfBirth = dataOfBirth;
    }
}


