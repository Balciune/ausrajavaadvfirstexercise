public class Trainer extends Person {

    private boolean isAuthorized;


    public Trainer(String firstName, String secondName, String dataOfBirth, boolean isAuthorized) {
        super(firstName, secondName, dataOfBirth);
        this.isAuthorized= isAuthorized;
    }

    public void setisAuthorized(boolean isAuthorized) {
        this.isAuthorized= isAuthorized;
    }

    public boolean getisAuthorized() {
        return this.isAuthorized;

    }
}