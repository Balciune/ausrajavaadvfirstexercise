import java.util.ArrayList;
import java.util.List;

public class Lists {
    private static List<Student> studentList = new ArrayList<>();

    static {

        Student Student1 = new Student("Rita", "Galdikiene", "1989.07.12", true);
        Student Student2 = new Student("Marius", "Riauka", "1965.12.12", true);
        Student Student3 = new Student("Tomas", "Kurpaitis", "1995.04.28", false);
        Student Student4 = new Student("Gita", "Tamosauskiene", "1973.11.02", false);
        Student Student5 = new Student("Laima", "Ruibyte", "1992.05.05", false);
        Student Student6 = new Student("Rokas", "Jasinskas", "1984.09.23", true);
        Student Student7 = new Student("Petras", "Rimavicius", "1997.06.18", false);
        Student Student8 = new Student("Mantas", "Gulbinas", "1989.02.04", true);
        Student Student9 = new Student("Meile", "Mazonyte", "1984.08.03", false);
        Student Student10 = new Student("Asta", "Baltrune", "1971.10.18", true);
        Student Student11 = new Student("Jonas", "Petraitis", "1962.03.12", false);
        Student Student12 = new Student("FirstNameStudent12", "SecondNameStudent12", "1986.09.22", false);
        Student Student13 = new Student("FirstNameStudent13", "SecondNameStudent13", "1991.11.01", false);
        Student Student14 = new Student("FirstNameStudent14", "SecondNameStudent14", "1990.12.04", true);
        Student Student15 = new Student("FirstNameStudent15", "SecondNameStudent15", "1993.07.17", false);

        studentList.add(Student1);
        studentList.add(Student2);
        studentList.add(Student3);
        studentList.add(Student4);
        studentList.add(Student5);
        studentList.add(Student6);
        studentList.add(Student7);
        studentList.add(Student7);
        studentList.add(Student8);
        studentList.add(Student9);
        studentList.add(Student10);
        studentList.add(Student11);
        studentList.add(Student12);
        studentList.add(Student13);
        studentList.add(Student14);
        studentList.add(Student15);

    }
    private static List<Trainer> trainerList = new ArrayList<>();

    static {

        Trainer Trainer1 = new Trainer("FirstNameTrainer1", "SecondNameTrainer2", "1988.04.15", true);
        Trainer Trainer2 = new Trainer("FirstNameTrainer2", "SecondNameTrainer2", "1974.11.04", true);
        Trainer Trainer3 = new Trainer("FirstNameTrainer3", "SecondNameTrainer3", "1961.08.24", true);

        trainerList.add(Trainer1);
        trainerList.add(Trainer2);
        trainerList.add(Trainer3);
    }


    private static List<Group> groupList = new ArrayList<>();

    static {

    Group Group1= new Group("Group1");
    Group Group2= new Group("Group2");
    Group Group3= new Group("Group3");
    Group Group4= new Group("Group4");

    groupList.add(Group1);
    groupList.add(Group2);
    groupList.add(Group3);
    groupList.add(Group4);
    
    }
}
