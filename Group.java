public class Group  {

    private String name;
    private Trainer trainer;

    public Group(String name) {
        this.name= name;
    }

    public Group() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Trainer getTrainer() {
        return trainer;
    }

    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }
//nepabaigta iki galo

}