public class Student extends Person {

    private boolean hasPreviousJavaKnowledge;

    public Student(String firstName, String secondName, String dataOfBirth, boolean hasPreviousJavaKnowledge) {
        super(firstName, secondName, dataOfBirth);
        this.hasPreviousJavaKnowledge = hasPreviousJavaKnowledge;
    }

    public void sethasPreviousJavaKnowledge(boolean hasPreviousJavaKnowledge) {
        this.hasPreviousJavaKnowledge = hasPreviousJavaKnowledge;
    }

    public boolean gethasPreviousJavaKnowledge() {
        return this.hasPreviousJavaKnowledge;

    }

}